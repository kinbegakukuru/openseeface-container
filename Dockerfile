## Kinbe Gakukuru (C) 2023
# Container is based on python 3.10 "Debian-Slim" image.  This could be ported to alpine in the future.
FROM python:3.10-slim

# Download and unzip openseeface (currently static, but will be dynamic in the future).
ADD https://github.com/emilianavt/OpenSeeFace/releases/download/v1.20.4/OpenSeeFace-v1.20.4.zip /tmp/
ADD unzip.py /tmp/unzip.py
RUN python3 /tmp/unzip.py
WORKDIR /openseeface

# Container requires libGL shared object libraries.
#   Current package list is as follows: 
#       - libgl1  - libglib2.0-0  - libsm6  - libxrender1  - libxext6
#   These packages need to be slimmed down to remove container bloat.

# Update apt/pip and create a virtualenv so pip doesn't yell at us.
RUN apt-get update && python3 -m venv env && pip install --upgrade pip 
RUN ["/bin/bash", "-c", "source env/bin/activate"]
RUN pip install onnxruntime opencv-python pillow numpy==1.22.4 && apt-get install libgl1 libglib2.0-0 libsm6 libxrender1 libxext6 -y 

# Run facetracker.py.  Camera and IP address are statically assigned and need dynamic replaceements in the future.  Current IP is default bridge host address.
CMD python3 facetracker.py -c 2 -W 1280 -H 720 --discard-after 0 --scan-every 0 --no-3d-adapt 1 --max-feature-updates 900 -i 172.17.0.1
