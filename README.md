## OpenSeeFace-Container

## Description
Dockerfile for running emilianavt's "OpenSeeFace" facetracker in a Docker/Podman compatible environment!
This container receives input from a `/dev/video` device, and outputs tracking data via udp (default port 11573)

This project is (nearly) self-contained in the Dockerfile, meaning the only installation requirement is a Docker-compatible runtime Linux environment.  That said, this project is still in early alpha, so many aspects are quite bloated and statically typed.  Please note that I am by no means a software engineer or computer scientist and my understanding of virtualenv, Python, shared object libraries, and containerization from a lower-level perspective is limited at best.

Current staticly configured items are:
- Docker bridge subnet (172.17.0.x/24)
- Video device number (/dev/video2)
- Forced installation of shared libraries

## Installation
Installation should be as simple as pulling the repo and building the container.  Be sure your local user has access to unprivlidged containers to avoid running as root.
Note: Current installation is approx 1.3GB due to lazy shared library installation (on my part).  Just think of it as a Daitoshokan!
```
$ git clone https://gitlab.com/kinbegakukuru/openseeface-container.git
$ cd openseeface-container
$ docker build . -t openseeface

```

## Usage
Container can be ran with or without sudo privileges, assuming non-root user has access to rootless containers or Docker runtime daemon.  Be sure to set the "Listen IP" of any connected software to the respective IP on the Docker bridge (i.e. 172.17.0.1 for Docker host), **not** localhost (i.e. 127.0.0.1).  Note that lower-quality USB hubs may periodically disconnect camera from host system causing container to restart or crash.  If this happens, try plugging camera directly into your host device.

Note: Camera device is hardcoded to /dev/video2 but can be editied in the Dockerfile `CMD` line as `facetracker.py [...] -c (CAMERA NUMBER)`.
```
$ docker run --rm --device=/dev/video2 -dit --name openseeface
```

## Roadmap
TODO: Swap static assignments to ENV variables and allow different docker networks.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Credit goes to upstream authors of the python container and emilianavt!

Original Project: https://github.com/emilianavt/OpenSeeFace

## License
This project is licensed under the BSD 2-Clause "Simplified" License.

## Project status
This project has no expected timeframe, and updates will happen randomly.
